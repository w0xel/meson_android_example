project('meson_android_example', 'cpp', version : '1.0.0')

is_android = meson.get_cross_property('is_android', false)
is_android_coordinator = false

# Pull in meson_android to employ its magic. Passing the name of the resulting libraries gives pathes to produced libs
if is_android
    android = subproject('meson_android', default_options: ['app_name='+meson.project_name()])
    is_android_coordinator = android.get_variable('is_coordinator')
endif

## WARNING: The android build is split into two Phases:
##   1. Coordinator: Nothing should be built, meson builds for the Android ABIs are prepared
##   2. Actual Build: The project is built once for each ABI, everything here is called again
# Do everything else in the project ONLY IF this is not the coordinator meson_android build
if not is_android_coordinator
    subdir('src')
    # ...
    # Put everything else apart from executables from the project also inside this if-block...
    # ...
endif

# If this is Android, build a shared library, else an executable as usual
if is_android
    # WARNING: Do not build a library unless this is the actual ABI build.
    if not is_android_coordinator
        # To make the target deps work, use the lib_name from the subproject. This will be app_name + _ + abi_suffix
        lib_name = android.get_variable('lib_name')
        shared_library(lib_name, dependencies: [app_dep, android.get_variable('android_app_dep')], install: true)
    else
        # this will only work if lib_name was used before. libs is an array of all your abi-specific libs.
        libs = android.get_variable('abi_libs')
        run_target('print_libs', command: ['file'] + libs)
        # ...
        # Here would be a good place to add custom targets for building an actual APK
        # ...
    endif
else
    executable(meson.project_name(), dependencies: [app_dep])
endif
